/* Name: main.c
 * Project: usb-hid-ds18b20
 * Author: Vencislav Atanasov
 * Creation Date: 2014-04-18
 * Tabsize: 4
 * Copyright (c) 2014 Vencislav Atanasov
 * License: GNU GPL v2 (see License.txt), GNU GPL v3 or proprietary (CommercialLicense.txt)
 */

/*
 * This project is based on hid-data, provided as an example of V-USB usage
 * This project uses code from V-USB (http://www.obdev.at/products/vusb/)
 *
 * This project is configured for ATtiny85 running on 16.5 MHz PLL clock
 * The pinout is as follows:
 * PB0 = Sensor
 * PB1 = LED
 * PB2 = Unused
 * PB3 = USB DATA -
 * PB4 = USB DATA +
 * PB5 = /RESET
 *
 * You can find the schematic for the board at http://digistump.com/products/1
 */

#include <stdint.h>

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>  /* for sei() */
#include <util/delay.h>     /* for _delay_ms() */

#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include "usbdrv.h"
#include "oddebug.h"        /* This is also an example for using debug macros */

#include "onewire.h"
#include "ds18x20.h"
#include "timer0.h"

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */

/* USB report descriptor */

PROGMEM const char usbHidReportDescriptor[22] = {
    0x05, 0x0c,       // USAGE_PAGE (Consumer Devices)
    0x0a, 0x05, 0x01, // USAGE (Room Temperature)
    0xa1, 0x01,       // COLLECTION (Application)
    0x0a, 0x05, 0x01, //   USAGE (Room Temperature)
    0x15, 0x00,       //   LOGICAL_MINIMUM (0)
    0x26, 0xff, 0x00, //   LOGICAL_MAXIMUM (255)
    0x75, 0x08,       //   REPORT_SIZE (8)
    0x95, 0x09,       //   REPORT_COUNT (9)
    0xb1, 0x02,       //   FEATURE (Data,Var,Abs)
    0xc0              // END_COLLECTION
};


/* USB report data */

volatile uint8_t usbHidData[9];
volatile uint8_t needToRead = 0;

/* ------------------------------------------------------------------------- */

usbMsgLen_t usbFunctionSetup(uchar data[8]) {
    usbRequest_t *rq = (void *)data;

    /* HID class request */
    if ((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS) {
        if (rq->bRequest == USBRQ_HID_GET_REPORT) {
            /* wValue: ReportType (highbyte), ReportID (lowbyte) */
            /* since we have only one report type, we can ignore the report-ID */
            usbMsgPtr = (usbMsgPtr_t)usbHidData;
            return sizeof(usbHidData);
        }
    }

    return 0;
}

void timer0_timer(void) {
    needToRead = 1;
}

/* ------------------------------------------------------------------------- */

int main(void) {
    uchar   i;

    wdt_enable(WDTO_1S);
    /* Even if you don't use the watchdog, turn it off here. On newer devices,
     * the status of the watchdog (on/off, period) is PRESERVED OVER RESET!
     */
    /* RESET status: all port bits are inputs without pull-up.
     * That's the way we need D+ and D-. Therefore we don't need any
     * additional hardware initialization.
     */
    timer0_init();
    timer0_set(0xA0);
    timer0_set_counter(100);

    odDebugInit();
    DBG1(0x00, 0, 0);       /* debug output: main starts */
    DDRB |= _BV(PB1);
    usbInit();
    usbDeviceDisconnect();  /* enforce re-enumeration, do this while interrupts are disabled! */
    i = 0;

    while(--i) {             /* fake USB disconnect for > 250 ms */
        wdt_reset();
        _delay_ms(1);
    }

    usbDeviceConnect();
    sei();
    timer0_start();
    DBG1(0x01, 0, 0);       /* debug output: main loop starts */

    while(1) {                /* main event loop */
        DBG1(0x02, 0, 0);   /* debug output: main loop iterates */
        wdt_reset();

        if (needToRead) {
            cli();
            DS18X20_read_scratchpad(NULL, (uint8_t *)usbHidData, 9);
            DS18X20_start_meas(DS18X20_POWER_PARASITE, NULL);
            needToRead = 0;
            sei();
        }

        usbPoll();
    }

    return 0;
}

/* ------------------------------------------------------------------------- */

void blinkLED() {
    PINB |= _BV(PB1);
}

/* ------------------------------------------------------------------------- */

// Called by V-USB after device reset
void hadUsbReset() {
    int frameLength, targetLength = (unsigned)(1499 * (double)F_CPU / 10.5e6 + 0.5);
    int bestDeviation = 9999;
    uchar trialCal, bestCal = 0, step, region;

    // do a binary search in regions 0-127 and 128-255 to get optimum OSCCAL
    for (region = 0; region <= 1; region++) {
        frameLength = 0;
        trialCal = (region == 0) ? 0 : 128;

        for (step = 64; step > 0; step >>= 1) {
            if (frameLength < targetLength) { // true for initial iteration
                trialCal += step; // frequency too low
            }
            else {
                trialCal -= step; // frequency too high
            }

            OSCCAL = trialCal;
            frameLength = usbMeasureFrameLength();

            if (abs(frameLength-targetLength) < bestDeviation) {
                bestCal = trialCal; // new optimum found
                bestDeviation = abs(frameLength -targetLength);
            }
        }
    }

    OSCCAL = bestCal;
}
