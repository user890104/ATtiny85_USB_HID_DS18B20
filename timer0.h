//
//
//    Copyright 2013 Vencislav Atanasov
//
//
//    This file is part of ATtiny85_USB_HID_DS18B20.
//
//    ATtiny85_USB_HID_DS18B20 is free software: you can redistribute it and/or
//    modify it under the terms of the GNU General Public License as
//    published by the Free Software Foundation, either version 2 of the
//    License, or (at your option) any later version.
//
//    ATtiny85_USB_HID_DS18B20 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//    See the GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License along
//    with ATtiny85_USB_HID_DS18B20. If not, see <http://www.gnu.org/licenses/>.
//
//

#ifndef TIMER0_H_
#define TIMER0_H_

#include <stdint.h>

#define TIMER0_PRESCALER 1024

void timer0_init(void);
void timer0_start(void);
void timer0_stop(void);
void timer0_set(uint8_t cycles);
void timer0_set_counter(uint16_t cycles);

#endif /* TIMER0_H_ */
