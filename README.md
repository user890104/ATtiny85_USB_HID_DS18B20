ATtiny85_USB_HID_DS18B20
========================

Firmware for ATtiny85 that interfaces with a temperatute sensor and outputs the result over USB HID.

It is configured for Digispark board.

http://digistump.com/products/1

PB0 = Sensor
PB1 = LED
PB2 = Unused
PB3 = USB DATA -
PB4 = USB DATA +
PB5 = /RESET

AVR Memory Usage
----------------
Device: attiny85

Program:    3006 bytes (36.7% Full)
(.text + .data + .bootloader)

Data:         71 bytes (13.9% Full)
(.data + .bss + .noinit)
