//
//
//    Copyright 2013 Vencislav Atanasov
//
//
//    This file is part of ATtiny85_USB_HID_DS18B20.
//
//    ATtiny85_USB_HID_DS18B20 is free software: you can redistribute it and/or
//    modify it under the terms of the GNU General Public License as
//    published by the Free Software Foundation, either version 2 of the
//    License, or (at your option) any later version.
//
//    ATtiny85_USB_HID_DS18B20 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//    See the GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License along
//    with ATtiny85_USB_HID_DS18B20. If not, see <http://www.gnu.org/licenses/>.
//
//

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timer0.h"

volatile uint16_t timer0_counter, timer0_counter_top;

extern void timer0_timer(void);

void timer0_init(void) {
    TCCR0A |= _BV(WGM01); // CTC mode, top = OCR0
    TIMSK |= _BV(OCIE0A); // Timer/Counter0, Compare Interrupt Enable
}

void timer0_start(void) {
#if TIMER0_PRESCALER == 1
    TCCR0B |= _BV(CS00);
#elif TIMER0_PRESCALER == 8
    TCCR0B |= _BV(CS01);
#elif TIMER0_PRESCALER == 64
    TCCR0B |= _BV(CS01) | _BV(CS00);
#elif TIMER0_PRESCALER == 256
    TCCR0B |= _BV(CS02);
#elif TIMER0_PRESCALER == 1024
    TCCR0B |= _BV(CS02) | _BV(CS00);
#endif
}

void timer0_stop(void) {
    TCCR0B &= ~(_BV(CS02) | _BV(CS01) | _BV(CS00));
}

void timer0_set(uint8_t cycles) {
    OCR0A = cycles;
}

void timer0_set_counter(uint16_t ten_ms_count) {
    timer0_counter_top = ten_ms_count;
}

ISR(TIMER0_COMPA_vect) {
    if (timer0_counter >= timer0_counter_top) {
        timer0_counter = 0;
        timer0_timer();
    }
    else {
        ++timer0_counter;
    }
}
